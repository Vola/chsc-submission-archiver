# CHSC Submission Archiver

Small Python script that works with DHT (dht.chylex.com) to archive score submissions.

To use:

- Create a directory for archives.
- Create a subdirectory for the week you're archiving.
- Use the script provided at dht.chylex.com to track all the submissions in a week's channels.
- Save the DHT file into the subdirectory created above.
- Fill out the ARCHIVE\_DIR, FOLDER\_NAME, and DHT\_FILE\_NAME variables in this script.
- Run it. 

This script will create a subdirectory for each channel (i.e. song), and download all the images submitted into that channel into its folder.

The images are named with the format `[user ID]+[username]+[timestamp].png`.

To see the current CHSC score submission archives, visit `rhyacian.com/f/CHSCArchives`