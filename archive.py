# -*- coding: utf-8 -*-
#
# Clone Hero Score Challenge Channel Screenshot Archival Tool
#
# First use Discord History Tracker (dht.chylex.com) to archive the channel(s), and use this tool to download all the
#   image files.
#
# Todo/nice-to-haves: automatic zipping, automatic upload to Rhyacian, replace this w/ a Discord bot

import os
import json
import requests

# Variable parameters - modify with each run (or quit being lazy and allow for command-line arguments)

ARCHIVE_DIR = "/path/to/folder"                 # Where archives are stored, absolute path
FOLDER_NAME = "CHSC Week XX"                    # This week's specific folder. Create this beforehand in ARCHIVE_DIR.
DHT_FILE_NAME = "dht.txt"                       # File name of DHT output. Make sure this is in FOLDER_NAME.


# Let's see if you fucked up the setup.
def sanity_check():

    if os.path.isfile(os.path.join(ARCHIVE_DIR, FOLDER_NAME, DHT_FILE_NAME)):
        pass

    else:
        raise FileNotFoundError("Check your file paths + names you donkey")     # Abusive code is best code.


# Extracts relevant channel and user info from deserealised JSON data, pulled from the dht file.
def get_metadata(dht):

    # DHT file uses an index to reference users in a list, so we can't use a dict (i.e. non-indexable) object alone for
    #     storing user ID and username pairs.
    # Instead we'll do what DHT does and keep a separate list to reference as well.

    userindex = dht["meta"]["userindex"]

    users, channels = {}, {}    # user ID : username; channel ID : channel name

    for u in dht["meta"]["users"]:
        users[u] = dht["meta"]["users"][u]["name"]

    for c in dht["meta"]["channels"]:
        channels[c] = dht["meta"]["channels"][c]["name"]

    return userindex, users, channels


# Get submission info for a given channel / song. Used for building image filenames and sifting out non-image messages.
def get_submissions(dht, channel, userindex, users):

    submissions = []
    message_ids = list(dht["data"][channel])

    for i in range(len(message_ids)):

        message_dict = {}

        content = dht["data"][channel][message_ids[i]]

        user_i = content['u']           # DHT references users by an index (i.e. int), so we grab that...
        user_id = userindex[user_i]     # Look it up in its userindex to get the user ID...
        username = users[user_id]       # Then look up the user name with the user ID.

        timestamp = content['t']

        # Most of this is actually unneeded - only the filename and URL are really needed to be stored.
        # But getting all relevant submission info into memory seemed like a good idea at the time.

        message_dict["ID"] = user_id
        message_dict["Name"] = username
        message_dict["Time"] = timestamp
        message_dict["Filename"] = ""  # placeholder
        message_dict['URL'] = ""  # placeholder

        # Submission images will either be attached ('a') or embedded ('e').
        # I'm sure there's a better way to handle this.
        if 'a' in content.keys():
            message_dict["URL"] = content['a'][0]['url']

        if 'e' in content.keys():
            message_dict["URL"] = content['e'][0]['url']

        message_dict["Filename"] = '{}+{}+{}.{}'.format(message_dict['ID'],
                                                        message_dict['Name'],
                                                        message_dict['Time'],
                                                        'png')

        submissions.append(message_dict)

    return submissions


# Where the magic happens.
def main():

    with open(os.path.join(ARCHIVE_DIR, FOLDER_NAME, DHT_FILE_NAME), "r") as dht:
        dht_data = json.load(dht)

    userindex, users, channels = get_metadata(dht_data)

    for channel in channels.keys():

        channel_name = channels[channel]
        channel_dir = os.path.join(ARCHIVE_DIR, FOLDER_NAME, channel_name)

        os.mkdir(channel_dir)

        submissions = get_submissions(dht_data, channel, userindex, users)

        for s in submissions:
            if s["URL"]:

                img_path = os.path.join(channel_dir, s["Filename"])
                img = requests.get(s["URL"])
                print("Downloading " + s["URL"])

                with open(img_path, 'wb') as imgfile:
                    imgfile.write(img.content)


main()

